# OAMS client samples

This repo contains samples of working OAMS client code for various languages. However before releasing a client that uses the public OAMS server, please contact the server operator via [email](mailto:nr0q@gridtracker.org) or in our [Discord](https://discord.gg/WBEbnChU8J).