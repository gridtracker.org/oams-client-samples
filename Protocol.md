# OAMS Client Protocol

## Client Data Template

- 'uuid':
- 'src': 2 char ID of the software client used (ie, GridTracker: "GT", Log4OM: "L4")
- 'ver': string ID of the software client version (ie "v1.21.1217")
- 'cid':
- 'call': string user's ham callsign
- 'grid': string user's maidenhead grid locator
- 'freq': integer user's radio frequency in Hz
- 'band': string user's radio band
- 'mode': string user's radio mode
- 'canmsg': is a boolean (true|false) if messaging is enabled
- 'o': is a boolean (true|false) if spotting is enabled or the spot JSON object

## Message Types / Payloads

### Send UUID

- type: "uuid"
- call:
- ver:

### Send Status

- type: "status"
- uuid:
- call:
- grid:
- freq:
- band: 
- src: "ID"
- canmsg: {true|false}
- o: {true|false}

### Send Spot

- type: "o"
- uuid: 
- o: object

### Request Online List

- type: "list"
- uuid:

### Send Chat Message

- type: "mesg"
- uuid:
- cid:
- msg: base64 encoded message text

### Receive Drop User

- type: "drop"
- cid:
